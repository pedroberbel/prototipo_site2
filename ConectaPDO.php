<?php
session_start();
// Biblioteca de funções de manipulação de cadastros
// Por Pedro Luis Berbel
#function connect()
#function isConnected($usr,$pass)
#function realizaCadastro($usr,$mail,$pass1,$pass2)
#function verificaCadastro($usr,$pass)
#function alteraSenha($usr,$newPass)

class ConectaPDO{  
 	//variaveis para conexão com banco de dados, recebidas ao instanciar a Classe.
	private $host;
	private $user;
	private $password;
	private $dbName;
	private $conexao;
	// private $usrConectado=0;
	// private $passConectado=0;

	public function __construct($host,$user,$pass,$dbname){
		$this->host = $host;
		$this->user = $user;
		$this->password = $pass;
		$this->dbName = $dbname;
	}

	public function connect(){
		// echo "oi<br>";
		try { //tenta fazer a conexão
   	 	$opLang = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8');
		$this -> conexao = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName,$this->user, $this->password, $opLang);

	} catch(PDOException $e) { 
		echo "error:" .$e->getMessage();
	}	//ok	
	}//ok

#function: isConnected()
public function isConnected($user,$password){  
	//fazer aqui a validação de usuario logado com o $_SESSION
	//sem aparecer quando o usuario ja esta logado
	echo "Entrou isConnected";
	$usuario = $user;
	$senha = $password;
	$_SESSION['logUser'] = $usuario;
	$_SESSION['logPass'] = $senha;
	if (isset($_SESSION['logUser']) and isset($_SESSION['logPass'])){
	$this->usrConectado = $usuario;
	$this->passConectado = $senha;
	// echo "<br>Redirecionando...<br>";
	header('Location: firstView.php');
	} //fim verificação para redirecionar.
}//ok

public function disconnect(){
		// if(isset($_POST[""]))
}

#function realizaCadastro
public function realizaCadastro($usuario,$mail,$senha1,$senha2){

		$usr = $usuario;
		$mail = $mail;
		$senha1 = $senha1;
		$senha2 = $senha2;



	#1- Verificar dados inseridos para cadastrado------
	$verificacao = FALSE;
	#0- Verificar se senha foi digitada
	# e confirmar se são iguais: 
        //trim() - remove espaços em branco
        //strip_tags() - remove codificações
        if ($senha1 == "" or $senha2 == "" or $usr == "" or $mail == "") { //acho que ja é tratado na passagem de parametro. Verificar.
            $mensagem = "<span class='aviso'><b>Aviso</b>: Campos Inválidos</span>";
            //  $botao = "<a href='cadastroView.html'>Voltar</a>";
                $verificacao = FALSE;

                } elseif ($senha1 == $senha2) {
            // $mensagem = "<span class='sucesso'><b>Sucesso</b>: As senhas são iguais: ".$senha1."</span>";
            //chama Classe de conexão
            $verificacao = TRUE;
        } else {
            $mensagem = "<span class='erro'><b>Erro</b>: As senhas não conferem!</span>";
           // $botao = "<a href='cadastroView.html'>Voltar</a>";
            $verificacao = FALSE;
        }
        echo "<p id='mensagem'>".$mensagem."</p>";
        echo $botao;
        // echo "<br>Verificação: $verificacao<br>";
	//------ FIM VERIFICAÇÃO de dados inseridos-- - -- 
	//------ VERIFICA SE USUARIO/EMAIL Já ESTAO CADASTRADOS: 
	if($verificacao == TRUE){
	$select = "SELECT user FROM cadastro WHERE user = :usr OR email = :mail";
	// echo "<br>Etapa pronta: SELECT<br>";

	try{
		// echo "Entrou no try<br>";
		$compara = $this->conexao->prepare($select);
		$compara->bindParam(':usr',$usr, PDO::PARAM_STR);
		$compara->bindParam(':mail',$mail, PDO::PARAM_STR);
		$compara->execute();
		$count=$compara->rowCount();
		// echo "<br>Verificação de Disponibilidade de Usuário:<br>";
		// echo "Qntidade: ".$count;
	if ($count > 0){
		echo "Usuário já cadastrado com este nome ou email: $count<br>";
	// return 0;
	} else {
    // echo "Usuário Disponível<br>";
	#2- Se não houver, realizar cadastro ------------
	try {
    $insert = "INSERT INTO cadastro(user, password, email) VALUES (:usr, :senha1 ,:mail)";
    $insere = $this->conexao->prepare($insert);
    $insere->bindParam(':usr',$usr, PDO::PARAM_STR);
    $insere->bindParam(':senha1',$senha1, PDO::PARAM_STR);
    $insere->bindParam(':mail',$mail, PDO::PARAM_STR);
    $insere->execute();
    echo "<br>Cadastro Realizado com Sucesso!<br>";
    // setUsuarioConectado($usr,$senha1); //envia quem está conectado
	return 1;

    } catch (PDOException $error){
   	 echo $error;
	}

	} //fim else

	} catch(PDOException $e) {
		// echo "<br>esrrinho maroto: ";
		echo $e;
	}
	}//fim if verificação TRUE

}//fim function verificaCadastro

			
#function: verificaCadastro
public function verificaCadastro($user,$password){
 	echo "<br>verificaCadastro() ativada...<br>";
 		$usr = $user;
 		$pass = $password;

 		if($usr == "" or $pass == ""){
 			echo "<br>Campos não preenchidos<br>";
 			$verificacao = FALSE;
 		} else {
 			$verificacao = TRUE;
 			//vai verificar se existe o usuario
 			echo "<br>Campos preenchidos ok:<br>";
 			echo "user: $usr<br>";
 			echo "senha: $pass<br>";

	// SE TRUE: Realiza LOGIN: 
	if($verificacao == TRUE){
	$select = "SELECT user FROM cadastro WHERE user = :usr AND BINARY password = :pass";
	echo "<br>Etapa pronta: SELECT<br>";
	try{
		// echo "Entrou no try<br>";
		$compara = $this->conexao->prepare($select);
		$compara->bindParam(':usr',$usr, PDO::PARAM_STR);
		$compara->bindParam(':pass',$pass, PDO::PARAM_STR);
		$compara->execute();
		$count=$compara->rowCount();
		if($count == 1){
			echo "<br>LOGIN VALIDADO<br>";
			
			return 1; //envia validação para redirecionamento
		} else {
			echo "<br>LOGIN INVALIDO<br>";
		}

 	} catch(PDOException $e){
 			echo $e;
 		}
    }//fim verificacao (IF)
	}//fim else: campos preenchidos
} //fim verificaCadastro()

public function alteraSenha($currentlyUser, $newPassword){
		echo "<br>senha nova: $newPassword e usuario: $currentlyUser<br>";
    $update = "UPDATE cadastro SET password = :newPassword WHERE user = :currentlyUser";
    try {
    $update = $this->conexao->prepare($update);
    $update->bindParam(':currentlyUser',$currentlyUser, PDO::PARAM_STR);
    $update->bindParam(':newPassword',$newPassword, PDO::PARAM_STR);
    $update->execute();
    echo "<br>Senha Alterada com Sucesso!<br>";
    $_SESSION['logPass'] = $newPassword;
	return 1;

    } catch (PDOException $error){
   	 echo $error;
   	 return 0;
	}
}

}	//fim classe
?>