<?php
ob_start();
session_start();

if (isset($_SESSION['logUser']) and isset($_SESSION['logPass'])){
    header('Location: firstView.php');
    }

require_once 'ConectaPDO.php';

// echo "<br>topo SESSAO:". $_SESSION['logUser'];
// echo "<br>";
?>
<!DOCTYPE html>
<html lang='pt'>
<head>
<?php
include('head-style.php');
// include('footer-style.php');

?>
<title>Login no Sistema</title>
</head>

<!-- STATIC BAR -->

<body>
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
      <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Site Protótipo</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li role="separator" class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="#">Separated link</a></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="cadastroView.php">Cadastrar</a></li>
            <li class="active"><a href="">Login<span class="sr-only">(current)</span></a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>


<!-- FORMULARIO DE LOGIN -->

    <div class="container">

      	<form class="form-signin" method="post">
      	<table border="1.0">
        <h2 class="form-signin-heading">Login</h2>

		<label for="inputUser" class="sr-only">User Name</label>
		<input type="text" id="firstField" class="form-control" name="user" placeholder="Usuário" required autofocus>
			

		<label for="inputPassword" class="sr-only">Password</label>
		<input type="password" id="lastField" class="form-control" name="pass" placeholder="Senha" require>
		<!-- <div class="checkbox">
          	<label>
            <input type="checkbox" value="remember-me"> Lembrar Dados
          	</label>
        </div> -->
		<button class="btn btn-lg btn-primary btn-block" type="submit" name="entrar">Entrar</button>
		<a href="recuperarSenha.php">Esqueci minha senha</a>
		<br><br>
	
  		 </table>
    	  </form>

   	</div> 
</body>  

<footer id="rodape">
<p class="fine">
©Copyright 2017 - by Pedro Luis Berbel<br>
<a href="http://facebook.com" target="_blank">Facebook</a> | <a href="http://twitter.com">Twitter</a>
</p>
</footer>

</html>

<?php
if(isset($_POST["entrar"])){
$conexao = new ConectaPDO('localhost','root','root','pedro1');
$conexao->connect();
// echo "apos connect";
$user = strip_tags(trim($_POST['user']));
$pass = strip_tags(trim($_POST['pass']));
$valida = $conexao->verificaCadastro($user,$pass);
// echo "<br>VALIDA: $valida<br>";
// echo "clicou e conectou";
	if($valida == 1){ //verificação de usuario - redireciona pagina de logado
	// $conexao->setUsuarioConectado($user,$pass);
	// echo "chama a funcao de SESSION:";
	$conexao -> isConnected($user,$pass);

	} else { 
		// echo "nao conectou";
		}
} //fim if do botão clicado de Entrar


?>