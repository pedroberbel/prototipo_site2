<?php
//NAV BAR STATIC - PADRAO

?>

<!-- <body> -->
<!-- STATIC BAR ON-LINE -->

<nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Site Protótipo</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <!-- Menus Específicos -->
            <li class="active"><a href="index.php">Home<span class="sr-only">(current)</span></a></li>
            <li><a href="aboutView.php">About</a></li>
            <li><a href="#contact">Contact</a></li>


          </ul>
          <ul class="nav navbar-nav navbar-right">

<!-- Início Dropdown -->
    <!-- Só aparece se usuario: 1-->
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Tema <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Cinza</a></li>
                <li><a href="#">Preto</a></li>
                <li><a href="#">Marrom</a></li>
                <li role="separator" class="divider"></li>
                <!-- <li class="dropdown-header">Nav header</li> -->
                <li>
                  <a href="#"><input type="checkbox">&nbsp&nbspParallax</a>
                </li>
                <!-- <li><a href="#">One more separated link</a></li> -->
              </ul>
</li><!-- fim dropdown -->
<!-- Só aparece se usuario: 0 -->
            <li><a href="cadastroView.php">Cadastrar</a></li>
            <li><a href="loginView.php">Login</a></li>
          </ul>
        </div><!-- /.nav-collapse -->
<!--           <p class='navbar-brand-sub'>oi2</p>
 -->      </div>
    <!-- </nav>  fim NAVBAR -->


