<?php
ob_start();
session_start();
$alertAlteraSenha = $_COOKIE['alert'];

// echo "ALERTA COOKIE: $alert";
if (!isset($_SESSION['logUser']) and !isset($_SESSION['logPass'])){
    header('Location: loginView.php');
}

require_once 'ConectaPDO.php';
?>
<!DOCTYPE html>
<html>
<head>
<?php
include('head-style.php');
?>
	<title>Titulo logado</title>
</head>
<body>
<?php
$logUser = $_SESSION['logUser'];
// echo "Usuário: ".$_SESSION['logUser'];
$conexao = new ConectaPDO();
// $usuarioConectado = $conexao -> getUsuarioConectado();
// echo "$usuarioConectado<br>";

?>
<!-- STATIC NAV BAR -->

<body>
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
      <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Site Protótipo</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php">Home </a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>



          </ul> 
          <ul class="nav navbar-nav navbar-right">
            <!-- Início Dropdown -->
    <!-- Só aparece se usuario: 1-->
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Tema <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Cinza</a></li>
                <li><a href="#">Preto</a></li>
                <li><a href="#">Marrom</a></li>
                <li role="separator" class="divider"></li>
                <!-- <li class="dropdown-header">Nav header</li> -->
                <li>
                  <a href="#"><input type="checkbox">&nbsp&nbspParallax</a>
                </li>
                <!-- <li><a href="#">One more separated link</a></li> -->
              </ul>
</li><!-- fim dropdown TEMA -->

<!-- DROPDOWN - CONFIGURAÇÕES USUÁRIO -->
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo "Usuário: $logUser"; ?><span class="caret"></span></a>

              <ul class="dropdown-menu">
                <li><a href="#">Dados Cadastrais</a></li>
                <li><a href="changePass.php">Alterar Senha</a></li>
                  <li role="separator" class="divider"></li>
                <li><form method="post"><button class="btn btn-lg btn-primary btn-block" type="submit" name="logout">Sair</button></form></li>

              </ul>
            </li>

<!--             <div class="input-group">
            	<input class="form-control" type="text" placeholder="Search" />
    <div class="input-group-btn">
        <button class="btn btn-default" type="submit">Search</button>
            </div> -->
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
   <!-- FINAL DA NAV BAR      -->

<?php if($alertAlteraSenha == 1){ ?>
 <div class="alert alert-success" role="alert">Senha alterada com sucesso!</div>
<?php } ?>

<h1> LOGADO COM SUCESSO!</h1>


  <a href="https://braveman.com.br/collections/frontpage/products/allblack-pack" class="action_button">
                MAIS
              </a>




<div id="shopify-section-1482528636777" class="shopify-section instagram-feed-section">
<div class="instagram-feed-wrap container">
  
    <div class="section clearfix">
      <div class="sixteen columns">
        <h4 class="title">
          
            <a href="http://instagram.com/bravemanlife" target="_blank">
          
          Instagram
          
            </a>
          
        </h4>

      <div id="instafeed" class="js-instafeed" data-client-id="825876637.d90570a.280e99abac19400095964a6e107197a0" data-count="5"></div>
    </div>
  </div>
  
</div>



</div><!-- END content_for_index -->



</form>
<?php
if(isset($_POST["logout"])){
echo "<br>Clicou SAIR<br>";
unset($_SESSION['logUser']);
unset($_SESSION['logPass']);
session_destroy();
    header('Location: loginView.php');
}
if(isset($_POST["saiuu"])){
	echo "SAIU";
}

?>



</body>
</htmll>