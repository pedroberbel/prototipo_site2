<?php 
ob_start();
session_start();
include('head-style.php');

if (isset($_SESSION['logUser']) and isset($_SESSION['logPass'])){
// header('Location: loginView.php');
// include('navbarConfig.php');
$navBar=1; 
} else {
// include ('navBarConfigOff.php');
$navBar=0;
}

?>
<!DOCTYPE html>
<html lang="pt">
<head>
<link href="css/carousel.css" rel="stylesheet">
</head>

<body>

<!-- STATIC BAR -->

<nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Site Protótipo</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
          
            <!-- Menus Específicos -->
            <li class="active"><a href="index.php">Home<span class="sr-only">(current)</span></a></li>
            <li><a href="aboutView.php">About</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
<?php
if ($navBar==1){ ?>
<!-- Só aparece se usuario: 0 -->
<!-- Início Dropdown -->
    <!-- Só aparece se usuario: 1-->
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Tema <span class="caret"></span></a>
              <ul class="dropdown-menu">
                
                <li>
                  <a href="#"><input type="checkbox" name="ativaParallax">&nbsp&nbspParallax</a>
                </li>
                <!-- <li><a href="#">One more separated link</a></li> -->
              </ul>
</li><!-- fim dropdown -->
            <li><a href="firstView.php">Minha Conta</a></li>
          
<?php } else { ?>
<!-- Só aparece se usuario: 0 -->
            <li><a href="cadastroView.php">Cadastrar</a></li>
            <li><a href="loginView.php">Login</a></li>
          
<?php } ?>
</ul>
        </div><!-- /.nav-collapse -->
<!--           <p class='navbar-brand-sub'>oi2</p>
 -->      </div>
    <!-- </nav>  fim NAVBAR -->


     <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="images/carousel1-corte.jpg" alt="First slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>Planta Marota.</h1>
              <p>Note: If you're viewing this page via a <code>file://</code> URL, the "next" and "previous" Glyphicon buttons on the left and right might not load/display properly due to web browser security rules.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img class="second-slide" src="images/carousel2-corte.jpg" alt="Second slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>Eletrônica I.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Assistir</a></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img class="third-slide" src="images/carousel3-corte.jpg" alt="Third slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>Eletrônica II.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Assistir</a></p>
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div><!-- /.carousel -->
    <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      <div class="row">
        <div class="col-lg-4">
          <img class="img-circle" src="images/headline1.jpg" alt="Generic placeholder image" width="140" height="140">
          <h2>Dog</h2>
          <p>Aqui pode-se inserir um texto a respeito do que você quer falar, pode ser qualquer coisa, e acima, uma imagem ilustando este texto.</p>
          <p><a class="btn btn-default" href="#" role="button">Mais Detalhes &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140">
          <h2>Heading</h2>
          <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh.</p>
          <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140">
          <h2>Heading</h2>
          <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="css/signin/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
